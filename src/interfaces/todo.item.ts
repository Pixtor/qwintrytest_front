export interface ITodoItem {
    id: number,
    title: string,
    done: boolean,
    urgent: boolean,
    selected?: boolean
}
